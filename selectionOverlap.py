import numpy as np
import matplotlib.pyplot as plt
import numpy.ma as ma
import sys

# ----- Input files
channel = "1L"
onlySRforResolved = True #works only for 2L and for 0L/1L data at the moment 
readData = True

if channel == "1L":
    if readData:
        filepathMerged  = 'inputFiles/1L/fileMergedData2018_010420.txt'
        #filepathResolved  = 'inputFiles/1L/fileResolvedData2018_010420.txt'
        filepathResolved  = 'inputFiles/1L/fileResolvedData2018SRCR_010420.txt' #numbers not coherent with file above
    else:
        filepathMerged  = 'inputFiles/fromBrian/fileMerged20200310.txt'   
        filepathResolved= 'inputFiles/fromBrian/fileResolvedDirectSRCR.txt' 
        #filepathResolved= 'inputFiles/fromBrian/fileResolvedDirectSR.txt'

if channel == "0L":
    if readData:
        # --- Data
        filepathResolved = 'inputFiles/fromGiulia/fileResolved_data18_200316.txt'
        filepathMerged   = 'inputFiles/fromGiulia/fileMerged_data18.txt'
    else:
        # --- Signal events
        #filepathResolved = 'inputFiles/fromGiulia/fileResolvedSROnly_mc16e_200316.txt'
        filepathResolved = 'inputFiles/fromGiulia/fileResolved_mc16e.txt'
        filepathMerged = 'inputFiles/fromGiulia/fileMerged_mc16e.txt'

if channel == "2L":
    if readData:
        # --- Data
        filepathResolved = 'inputFiles/fromWeitao/SRCRResolved/fileResolved_alldata.txt'     
        filepathMerged = 'inputFiles/fromWeitao/SRCRResolved/fileMerged_alldata.txt'
    else:
        # --- Signal events
        filepathMerged  = 'inputFiles/fromWeitao/SRCRResolved/fileMerged_signal_e.txt'     
        filepathResolved= 'inputFiles/fromWeitao/SRCRResolved/fileResolved_signal_e.txt' 
#------------ read Resolved and Merged files ------------------------------------------------------- 
# Warning: EvtNum can be the same for two MC simulation, it is better to use only the main signal for each channel. 
# In 1L WpH and WmH events are then selected separately to avoid fake overlaps

if channel == "0L":
    if readData:
        DSID, EvtNum, ptv, weight, ptvReco, region = np.genfromtxt(filepathResolved, missing_values='0', delimiter=',', unpack = True, invalid_raise = False)
        resoVH = np.vstack((DSID, EvtNum, ptv, weight, ptvReco, region))
    else:
        DSID, EvtNum, ptv, weight, ptvReco = np.genfromtxt(filepathResolved, missing_values='0', delimiter=',', unpack = True, invalid_raise = False)
        resoVH = np.vstack((DSID, EvtNum, ptv, weight, ptvReco))

    DSID, EvtNum, ptv, weight, ptvReco = np.genfromtxt(filepathMerged, missing_values='0', delimiter=',', unpack = True, invalid_raise = False)
    mergVH = np.vstack((DSID, EvtNum, ptv, weight, ptvReco))

if channel == "1L":
    if readData:
        DSID, EvtNum, ptv, weight, ptvReco, region = np.genfromtxt(filepathResolved, missing_values='0', delimiter=',', unpack = True, invalid_raise = False)
        resoVH = np.vstack((DSID, EvtNum, ptv, weight, ptvReco, region))

        DSID, EvtNum, ptv, weight, ptvReco, region = np.genfromtxt(filepathMerged,  missing_values='0', delimiter=',', unpack = True, invalid_raise = False)
        mergVH = np.vstack((DSID, EvtNum, ptv, weight, ptvReco, region))
    
    else:
        # DSID Number 345054 and 345053 allow to separate WpH and WmH events    
        # Here 345054 events are ignored and only 345053 are saved, the opposite happens below
        DSID, EvtNum, ptv, weight, ptvReco = np.genfromtxt(filepathResolved, comments = '345054,', missing_values='0', delimiter=',', unpack = True, invalid_raise = False)
        resoWpH = np.vstack((DSID, EvtNum, ptv, weight, ptvReco))
        DSID, EvtNum, ptv, weight, ptvReco = np.genfromtxt(filepathResolved, comments = '345053,', missing_values='0', delimiter=',', unpack = True, invalid_raise = False)
        resoWmH = np.vstack((DSID, EvtNum, ptv, weight, ptvReco))

        DSID, EvtNum, ptvWpH, weightWpH, ptvReco = np.genfromtxt(filepathMerged, comments = '345054,', missing_values='0', delimiter=',', unpack = True, invalid_raise = False)
        mergWpH = np.vstack((DSID, EvtNum, ptvWpH, weightWpH, ptvReco))
        DSID, EvtNum, ptvWmH, weightWmH, ptvReco = np.genfromtxt(filepathMerged, comments = '345053,', missing_values='0', delimiter=',', unpack = True, invalid_raise = False)
        mergWmH = np.vstack((DSID, EvtNum, ptvWmH, weightWmH, ptvReco))

if channel == "2L":

    DSID, EvtNum, weight, ptv, ptvReco, truthwzPTH, kfPTH, mBB, region = np.genfromtxt(filepathResolved, missing_values='0', delimiter=',', unpack = True, invalid_raise = False)
    resoVH = np.vstack((DSID, EvtNum, weight, ptv, ptvReco, truthwzPTH, kfPTH, mBB, region))

    #------------ read Merged file ----------------------------------
    DSID, EvtNum, weight, ptv, ptvReco, truthwzPTH, kfPTH, mBB, region = np.genfromtxt(filepathMerged, missing_values='0', delimiter=',', unpack = True, invalid_raise = False)
    mergVH = np.vstack((DSID, EvtNum, weight, ptv, ptvReco, truthwzPTH, kfPTH, mBB, region))

if channel == "0L" or channel == "2L" or ( channel == "1L" and readData ):

    resolved = np.transpose(resoVH)
    merged = np.transpose(mergVH)

    # discard CR for resolved
    if onlySRforResolved:
        if channel == "2L":
            indexR=[]
            for i in range(0,len(resolved[:,1] )):
                if resolved[i,8] == 0: continue
                indexR.append(i)
            TresolvedZH=[]
            for i in indexR:
                TresolvedZH.append(resolved[i,:])
            resolved=np.array(TresolvedZH)
        if readData and (channel == "0L" or channel == "1L"):
            indexR=[]
            for i in range(0,len(resolved[:,1] )):
                if resolved[i,5] == 0: continue
                indexR.append(i)
            TresolvedZH=[]
            for i in indexR:
                TresolvedZH.append(resolved[i,:])
            resolved=np.array(TresolvedZH)

    # remove events below 250 GeV
    indexM=[]
    for i in range(0,len(merged[:,1] )):
        if merged[i,4]<250: continue
        indexM.append(i)
    TmergedZH=[]
    for i in indexM:
        TmergedZH.append(merged[i,:])
    merged=np.array(TmergedZH)

    indexR=[]
    for i in range(0,len(resolved[:,1] )):
        if resolved[i,4]<250: continue
        indexR.append(i)
    TresolvedZH=[]
    for i in indexR:
        TresolvedZH.append(resolved[i,:])
    resolved=np.array(TresolvedZH)


    #------------ Compute overlap events ------------------------------------------
    # this function requires an updated version of numpy
    evtNumberOverlap, indexReso, indexMerged = np.intersect1d(resolved[:,1], merged[:,1], return_indices=True)
    resolvedEvents = len(resolved[:,1])
    mergedEvents   = len(merged[:,1])
    overlapEvents = len(evtNumberOverlap)

    #------ Extract overlap matrix, both using merged and resolved weights 
    overlapMergedW = []
    for i in indexMerged:
        overlapMergedW.append(merged[i,:])

    overlapResoW = []
    for i in indexReso:
        overlapResoW.append(resolved[i,:])

if (channel == "1L" and (not readData) ):
    mergedWpH = np.transpose(mergWpH)
    resolvedWpH = np.transpose(resoWpH)
    resolvedWmH = np.transpose(resoWmH)
    mergedWmH = np.transpose(mergWmH)

    indexM=[]
    for i in range(0,len(mergedWpH[:,1] )):
        if mergedWpH[i,4]<250: continue
        indexM.append(i)
    TmergedWpH=[]
    for i in indexM:
        TmergedWpH.append(mergedWpH[i,:])
    mergedWpH=np.array(TmergedWpH)

    indexM=[]
    for i in range(0,len(mergedWmH[:,1] )):
        if mergedWmH[i,4]<250: continue
        indexM.append(i)
    TmergedWmH=[]
    for i in indexM:
        TmergedWmH.append(mergedWmH[i,:])
    mergedWmH=np.array(TmergedWmH)


    indexR=[]
    for i in range(0,len(resolvedWpH[:,1] )):
        if resolvedWpH[i,4]<250: continue
        indexR.append(i)
    TresolvedWpH=[]
    for i in indexR:
        TresolvedWpH.append(resolvedWpH[i,:])
    resolvedWpH=np.array(TresolvedWpH)

    indexR=[]
    for i in range(0,len(resolvedWmH[:,1] )):
        if resolvedWmH[i,4]<250: continue
        indexR.append(i)
    TresolvedWmH=[]
    for i in indexR:
        TresolvedWmH.append(resolvedWmH[i,:])
    resolvedWmH=np.array(TresolvedWmH)
    
    #------------ Sum WpH and WmH events -----------------------------------------------------------------------------
    resolved = np.vstack( (resolvedWpH, resolvedWmH))
    merged   = np.vstack( (mergedWpH  , mergedWmH  ))

    #------------ Compute overlap events -----------------------------------------------------------------------------
    # this function requires an updated version of numpy
    evtNumberOverlapWpH, indexResoWpH, indexMergedWpH = np.intersect1d(resolvedWpH[:,1], mergedWpH[:,1], return_indices=True)
    evtNumberOverlapWmH, indexResoWmH, indexMergedWmH = np.intersect1d(resolvedWmH[:,1], mergedWmH[:,1], return_indices=True)
    resolvedEvents = len(resolvedWpH[:,1])+len(resolvedWmH[:,1])
    mergedEvents   = len(mergedWpH[:,1])  +len(mergedWmH[:,1])
    overlapEvents = len(evtNumberOverlapWpH)+len(evtNumberOverlapWmH)

    #------ Extract overlap matrix, both using merged and resolved weights 
    overlapMergedW = []
    for i in indexMergedWpH:
        overlapMergedW.append(mergedWpH[i,:])
    for i in indexMergedWmH:
        overlapMergedW.append(mergedWmH[i,:])

    overlapResoW = []
    for i in indexResoWpH:
        overlapResoW.append(resolvedWpH[i,:])
    for i in indexResoWmH:
        overlapResoW.append(resolvedWmH[i,:])

totalEvents = resolvedEvents + mergedEvents - overlapEvents
# Print the number of resolved and merged Entries (not weighted events)
print( "Resolved entries:\t", resolvedEvents)
print( "Merged entries:\t", mergedEvents)
print( "Overlap entries:\t", overlapEvents )

overlapMatrixMergedW = np.array(overlapMergedW)
overlapMatrixResoW = np.array(overlapResoW)

#----- plot reco ptv distributions
rangePTV = [250, 800]
binwidth = 25
if readData:
    binwidth = 50
binsPTV = int((800-250)/binwidth)
outputDir = "outputFiles/"+channel+"/"

# -------- Histo drawing ----------

if channel == "0L" or channel == "1L":
    # ptv for merged, resolved and overlap
    res, bins, patch3= plt.hist(resolved[:,4]          , binsPTV, range = rangePTV, label='Resolved'          , weights = resolved[:,3]          , histtype='step', log=True)
    mer, bins, patch1= plt.hist(merged[:,4]            , binsPTV, range = rangePTV, label='Merged'            , weights = merged[:,3]            , histtype='step', log=True)
    ovR, bins, patch2= plt.hist(overlapMatrixResoW[:,4], binsPTV, range = rangePTV, label='Overlap - Rweights', weights = overlapMatrixResoW[:,3], histtype='step', log=True)
    ovM, bins, patch2= plt.hist(overlapMatrixMergedW[:,4]     , binsPTV, range = rangePTV, label='Overlap - Mweights', weights = overlapMatrixMergedW[:,3]     , histtype='step', log=True)
else:
    # ptv for merged, resolved and overlap
    res, bins, patch3= plt.hist(resolved[:,4]          , binsPTV, range = rangePTV, label='Resolved'          , weights = resolved[:,2]          , histtype='step', log=True)
    mer, bins, patch1= plt.hist(merged[:,4]            , binsPTV, range = rangePTV, label='Merged'            , weights = merged[:,2]            , histtype='step', log=True)
    ovR, bins, patch2= plt.hist(overlapMatrixResoW[:,4], binsPTV, range = rangePTV, label='Overlap - Rweights', weights = overlapMatrixResoW[:,2], histtype='step', log=True)
    ovM, bins, patch2= plt.hist(overlapMatrixMergedW[:,4]     , binsPTV, range = rangePTV, label='Overlap - Mweights', weights = overlapMatrixMergedW[:,2]     , histtype='step', log=True)
plt.gca().set(title='V $p_T$ distributions', ylabel='yields', xlabel='V $p_T$ [GeV]');
plt.legend(loc="upper right")
plt.savefig(outputDir+"Reco_Vpt.pdf")
plt.close()

# merged and resolved weights
plt.hist(overlapMatrixResoW[:,3], 50, range = [-0.001,0.001], label='Rweights', histtype='step', log=True)
plt.hist(overlapMatrixMergedW[:,3]     , 50, range = [-0.001,0.001], label='Mweights', histtype='step', log=True)
plt.gca().set(title='Resolved and Merged weights', ylabel='yields', xlabel='weights');
plt.legend()
plt.savefig(outputDir+"Weights.pdf")
plt.close()

#center the data point
print (bins)
bins2=[]
for b in bins: bins2.append(b+(binwidth/2))
bins=bins2

#Fraction overlap in merged and resolved events
plt.plot(bins[:-1], (ovM)/(mer), 'r', label="fraction of overlap in merged"  )
plt.plot(bins[:-1], (ovR)/(res), 'b', label="fraction of overlap in resolved")
plt.gca().set(title='Fraction overlap in merged and resolved events', ylabel='Fraction of events', xlabel='V $p_T$ [GeV]');
plt.axis([250, 800, 0, 1.0])
plt.legend()
plt.savefig(outputDir+"Reco_Vpt_fractionOverlap.pdf")
plt.close()

plt.plot(bins[:-1], (mer-ovM)/(res)*100, 'r', label=" " )
plt.gca().set(title='% Fraction of resolved events uniquely added by merged', ylabel='% of resolved events', xlabel='V $p_T$ [GeV]');
plt.axis([250, 800, 0, 200.0])
plt.legend()
plt.savefig(outputDir+"Reco_Vpt_AddedMergedEvents.pdf")
plt.close()

plt.plot(bins[:-1], (res-ovR)/(mer)*100, 'r', label=" " )
plt.gca().set(title='% Fraction of merged events uniquely added by resolved', ylabel='% of merged events', xlabel='V $p_T$ [GeV]');
plt.axis([250, 800, 0, 200.0])
plt.legend()
plt.savefig(outputDir+"Reco_Vpt_AddedResolvedEvents.pdf")
plt.close()

plt.close()
plt.plot(bins[:-1], (ovM)/(ovR)-1, 'r', label=" " )
plt.gca().set(title=' comparison of overlap events with different weights', ylabel='Merged weights/Resolved weights', xlabel='V $p_T$ [GeV]');
plt.axis([250, 800, -0.1, 0.1])
plt.legend()
plt.savefig(outputDir+"Reco_Vpt_Overlap.pdf")
plt.close()

# fraction of resolved, merged and overlap over total events
plt.plot( bins[:-1], (res-ovM)/((mer-ovM)+res), 's', color='blue', label="res only")
plt.plot( bins[:-1], (ovM)/((mer-ovM)+res), '^', color='green', label="res AND mer")
plt.plot( bins[:-1], (mer-ovM)/((mer-ovM)+res), 'o', color='red', label="mer only")
plt.gca().set(title='Fraction of total events', ylabel='Fraction of events', xlabel='$p_T^{V}$ [GeV]');
plt.axis([250, 800, 0, 1.0])
plt.legend()
plt.savefig(outputDir+"Reco_Vpt_eventFraction.pdf")
plt.close()

# same, draws as stack
tmpB=bins
del tmpB[-1]
plt.hist( 
     [tmpB,tmpB,tmpB], binsPTV, range = rangePTV,  weights= [(res-ovM)/(mer-ovM+res), (ovM)/(mer-ovM+res), (mer-ovM)/(mer-ovM+res) ], 
     stacked=True, label=["res only", "res AND mer", "mer only"])
plt.gca().set(title='Fraction of total events', ylabel='Fraction of events', xlabel='$p_T^{V}$ [GeV]');
plt.legend()
plt.savefig(outputDir+"Reco_Vpt_Stack.pdf")
plt.close()

#----- plot truth ptv distributions
rangePTV = [0, 2000]
binsPTV = 200

plt.hist(merged[:,2], binsPTV, range = rangePTV, label='Merged', weights = merged[:,3], histtype='step')
plt.gca().set(title='qqWH, $p_T^{truth V}$ distributions', ylabel='Weighted events', xlabel='$p_T^{truth V}$ [GeV]');
plt.legend()
plt.savefig(outputDir+"merged.pdf")
plt.close()

plt.hist(overlapMatrixMergedW[:,2], binsPTV, range = rangePTV, label='Overlap - merged weights', weights = overlapMatrixMergedW[:,3], histtype='step')
plt.hist(overlapMatrixResoW[:,2], binsPTV, range = rangePTV, label='Overlap - resolved weights', weights = overlapMatrixResoW[:,3], histtype='step')
plt.gca().set(title='qqWH, $p_T^{truth V}$ distributions', ylabel='Weighted events', xlabel='$p_T^{truth V}$ [GeV]');
plt.legend()
plt.savefig(outputDir+"overlap.pdf")
plt.close()

plt.hist(resolved[:,2], binsPTV, range = rangePTV, label='Resolved', weights = resolved[:,3], histtype='step')
plt.gca().set(title='qqWH, $p_T^{truth V}$ distributions', ylabel='Weighted events', xlabel='$p_T^{truth V}$ [GeV]');
plt.legend()
plt.savefig(outputDir+"resolved.pdf")
plt.close()

plt.hist(resolved[:,2], bins=50, range=[150, 800], label='Resolved', alpha = 0.5, weights = resolved[:,3])
plt.hist(merged[:,2], bins=50, range=[150, 800], label='Merged', alpha = 0.5, weights = merged[:,3])
plt.hist(overlapMatrixMergedW[:,2], bins=50, range=[150, 800], label='Overlap', alpha = 0.5, weights = overlapMatrixMergedW[:,3])
plt.gca().set(title='qqWH, $p_T^{truth V}$ distributions', ylabel='Weighted events', xlabel='$p_T^{truth V}$ [GeV]');
plt.legend()
plt.savefig(outputDir+"pTV.pdf")
plt.close()

plt.hist(resolved[:,2], bins=50, range=[150, 800], density = 1, label='resolved', alpha = 0.5, weights = resolved[:,3])
plt.hist(merged[:,2], bins=50, range=[150, 800], density = 1, label='merged', alpha = 0.5, weights = merged[:,3])
plt.hist(overlapMatrixMergedW[:,2], bins=50, range=[150, 800], density = 1, label='overlap', alpha = 0.5, weights = overlapMatrixMergedW[:,3])
plt.gca().set(title='qqWH, $p_T^{truth V}$ distributions', ylabel='Weighted events', xlabel='$p_T^{truth V}$ [GeV]');
plt.legend()
plt.savefig(outputDir+"pTV_norm.pdf")
plt.close()

# overlap weight distribution
plt.hist(overlapMatrixMergedW[:,3], bins=100)
plt.gca().set(title='Overlap events', ylabel='Frequency', xlabel='weights');
plt.tick_params(axis='x', which='major', pad=10)
plt.xlim(-0.0007, 0.0007)
plt.savefig(outputDir+"weights.pdf")
plt.close()


# Plot the fraction of merged, resolved, overlap events as a function of ptv at RECO level
plt.close()
binningSTXS=[250, 400, 2000]
plt.hist(resolved[:,4], bins=binningSTXS, label='resolved events', weights = resolved[:,3], histtype='step')
plt.hist(merged[:,4], bins=binningSTXS, label='merged events', weights = merged[:,3], histtype='step')
plt.hist(overlapMatrixMergedW[:,4], bins=binningSTXS, label='overlap events', weights = overlapMatrixMergedW[:,3], histtype='step')
plt.gca().set(title='Events distribution', ylabel='Weighted events', xlabel='$p_T^V$ [GeV]');
plt.legend()
plt.savefig(outputDir+"eventsDistribution.pdf")
plt.close()

fig, (ax1, ax2) = plt.subplots(nrows=2)

ax1.set_xlim([250, 2000])
res, bins, patch3 = ax1.hist(resolved[:,4], binningSTXS, label='resolved events', weights = resolved[:,3], histtype='step')
mer, bins, patch1 = ax1.hist(merged[:,4], binningSTXS, label='merged events', weights = merged[:,3], histtype='step')
ove, bins, patch2 = ax1.hist(overlapMatrixMergedW[:,4], binningSTXS, label='overlap events', weights = overlapMatrixMergedW[:,3], histtype='step')
if channel == "2L":
    res, bins, patch3 = ax1.hist(resolved[:,4], binningSTXS, label='resolved events', weights = resolved[:,2], histtype='step')
    mer, bins, patch1 = ax1.hist(merged[:,4], binningSTXS, label='merged events', weights = merged[:,2], histtype='step')
    ove, bins, patch2 = ax1.hist(overlapMatrixMergedW[:,4], binningSTXS, label='overlap events', weights = overlapMatrixMergedW[:,2], histtype='step')
ax1.legend()

ax2.plot([325, 700], (mer-ove)/(mer-ove+res), 'ro', label="only merged")
ax2.plot([325, 700], (res-ove)/(mer-ove+res), 'bs', label="only resolved")
ax2.plot([325, 700], ove/(mer-ove+res), 'g^', label="overlap")
ax2.axis([250, 2000, 0, 1])
ax1.set_ylabel('Events')
ax2.set_ylabel('fraction of events')
ax2.legend()
fig.savefig(outputDir+"ratio.pdf")

plt.close()

#Integrals
if channel == "0L" or channel == "1L":
    print("Resolved")
    print("> 250 GeV:\t %.3f" % np.sum(resolved[:,3]))
    print("[250;400] GeV:\t %.3f " % res[0] )
    print("> 400 GeV:\t %.3f" % res[1] )
    print("%.3f %.3f %.3f" % ( np.sum(resolved[:,3]), res[0],  res[1]) )
    print("Boosted")
    print("> 250 GeV:\t %.3f" % np.sum(merged[:,3]))
    print("[250;400] GeV:\t %.3f " % mer[0] )
    print("> 400 GeV:\t %.3f" % mer[1] )
    print("%.3f %.3f %.3f" % ( np.sum(merged[:,3]), mer[0],  mer[1]) )
    print("Overlap")
    print("> 250 GeV:\t %.3f " % np.sum(overlapMatrixMergedW[:,3]))
    print("[250;400] GeV:\t %.3f " % ove[0] )
    print("> 400 GeV:\t %.3f " % ove[1] ) 
    print("%.3f %.3f %.3f" % ( np.sum(overlapMatrixMergedW[:,3]), ove[0],  ove[1]) )
if channel == "2L":
    print("Resolved")
    print("> 250 GeV:\t %.3f" % np.sum(resolved[:,2]))
    print("[250;400] GeV:\t %.3f " % res[0] )
    print("> 400 GeV:\t %.3f" % res[1] )
    print("%.3f %.3f %.3f" % ( np.sum(resolved[:,2]), res[0],  res[1]) )
    print("Boosted")
    print("> 250 GeV:\t %.3f" % np.sum(merged[:,2]))
    print("[250;400] GeV:\t %.3f " % mer[0] )
    print("> 400 GeV:\t %.3f" % mer[1] )
    print("%.3f %.3f %.3f" % ( np.sum(merged[:,2]), mer[0],  mer[1]) )
    print("Overlap")
    print("> 250 GeV:\t %.3f " % np.sum(overlapMatrixMergedW[:,2]))
    print("[250;400] GeV:\t %.3f " % ove[0] )
    print("> 400 GeV:\t %.3f " % ove[1] ) 
    print("%.3f %.3f %.3f" % ( np.sum(overlapMatrixMergedW[:,2]), ove[0],  ove[1]) )


fig, (ax1, ax2) = plt.subplots(nrows=2)

rangeRatio =[150, 800]

ax1.set_xlim(rangeRatio)
res, bins, patch3 = ax1.hist(resolved[:,2], bins=50, range = rangeRatio, label='resolved events', weights = resolved[:,3], histtype='step')
mer, bins, patch1 = ax1.hist(merged[:,2], bins=50, range = rangeRatio, label='merged events', weights = merged[:,3], histtype='step')
ove, bins, patch2 = ax1.hist(overlapMatrixMergedW[:,2], bins=50, range = rangeRatio, label='overlap events, merged weights', weights = overlapMatrixMergedW[:,3], histtype='step')
oveR, bins, patch2 = ax1.hist(overlapMatrixResoW[:,2], bins=50, range = rangeRatio, label='overlap events, resolved weights', weights = overlapMatrixResoW[:,3], histtype='step')


ax1.legend()

ax2.plot(bins[:-1], (mer-ove)/(mer-ove+res), 'ro', label="only merged")
ax2.plot(bins[:-1], (res-ove)/(mer-ove+res), 'bs', label="only resolved")
ax2.plot(bins[:-1], ove/(mer-ove+res), 'g^', label="overlap")
ax2.plot(bins[:-1], (mer-ove+res)/(mer-ove+res), 'mx', label="sum")
ax2.axis([150, 800, 0, 1.1])
ax1.set_ylabel('Events')
ax2.set_ylabel('Fraction of events')
ax2.legend()
fig.savefig(outputDir+"ratioptv.pdf")
plt.close()

sys.exit()

# overlap study using different weights
plt.plot(bins[:-1], (ove-oveR)/(oveR), 'ro', label=" (OverlMerge - OverlReso) / OverlReso")
plt.gca().set(ylabel='relative difference', xlabel='$p_T^{truth V}$ [GeV]');
plt.axis([150, 800, 0, 1.1])
plt.legend()
plt.savefig(outputDir+"overlapRelativeDifference.pdf")
plt.close()


# fraction of overlap events in resolved and merged

plt.close()
plt.plot(bins[:-1], (ove)/(mer), 'ro', label="fraction of overlap in merged")
plt.plot(bins[:-1], (oveR)/(res), 'bs', label="fraction of overlap in resolved")
plt.gca().set(title='Fraction of merged and resolved events in overlap', ylabel='Fraction of events', xlabel='$p_T^{truth V}$ [GeV]');
plt.axis([150, 800, 0, 1.1])
plt.legend()
plt.savefig(outputDir+"fractionOverlap.pdf")
plt.close()

plt.close()
plt.plot(bins[:-1], (oveR)/(mer), 'ro', label="fraction of overlap in merged")
plt.plot(bins[:-1], (oveR)/(res), 'bs', label="fraction of overlap in resolved")
plt.gca().set(title='Fraction of merged and resolved events in overlap, resolved weights', ylabel='Fraction of events', xlabel='$p_T^{truth V}$ [GeV]');
plt.axis([150, 800, 0, 1.1])
plt.legend()
plt.savefig(outputDir+"fractionOverlapResoW.pdf")
plt.close()

plt.close()
plt.plot(bins[:-1], (ove)/(mer), 'ro', label="fraction of overlap in merged")
plt.plot(bins[:-1], (ove)/(res), 'bs', label="fraction of overlap in resolved")
plt.gca().set(title='Fraction of merged and resolved events in overlap, merged weights', ylabel='Fraction of events', xlabel='$p_T^{truth V}$ [GeV]');
plt.axis([150, 800, 0, 1.1])
plt.legend()
plt.savefig(outputDir+"fractionOverlapMergeW.pdf")
plt.close()

plt.close()
plt.plot(bins[:-1], (mer-ove)/(mer-ove+res), 'ro', label="only merged")
plt.plot(bins[:-1], (res-ove)/(mer-ove+res), 'bs', label="only resolved")
plt.plot(bins[:-1], ove/(mer-ove+res), 'g^', label="overlap")
plt.plot(bins[:-1], (mer-ove+res)/(mer-ove+res), 'mx', label="sum")
plt.gca().set(title='Fraction of all selected events', ylabel='Fraction of events', xlabel='$p_T^{truth V}$ [GeV]');
plt.axis([150, 800, 0, 1.1])
plt.legend()
plt.savefig(outputDir+"ratioptvtruth.pdf")
plt.close()

plt.close()
plt.plot(bins[:-1], (mer-oveR)/(mer-oveR+res), 'ro', label="only merged")
plt.plot(bins[:-1], (res-oveR)/(mer-oveR+res), 'bs', label="only resolved")
plt.plot(bins[:-1], oveR/(mer-oveR+res), 'g^', label="overlap")
plt.plot(bins[:-1], (mer-ove+res)/(mer-ove+res), 'mx', label="sum")
plt.gca().set(title='Fraction of all selected events', ylabel='Fraction of events', xlabel='$p_T^{truth V}$ [GeV]');
plt.axis([150, 800, 0, 1.1])
plt.legend()
plt.savefig(outputDir+"ratioptvtruthResolvedWeights.pdf")
plt.close()

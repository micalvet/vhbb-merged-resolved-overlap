**Run the script**  
`python3 selectionOverlap.py`  
Running the script requires an updated version of numpy. When comparing to arrays, the updated version allows to save the index of the overlapping elements.   
If you have in mind equivalent or better solution to save the overlapping the events please update the code.  

**How to save the text files**   
in `CxAODReader_VHbb/Root/AnalysisReader_VHQQ1Lep.cxx` (or equivalent for other channels)
add this code within the function saving histograms:
```
  if (m_doOnlyInputs && fatJet.M() / 1e3 >= 50 && m_currentVar == "Nominal") {
    ofstream fileMerged;
    fileMerged.open("fileMerged.txt", std::ios_base::app);
    // DSID, EventNumber, truth ptv, eventweight, recoptv                                                                                                                                                 
    fileMerged << m_eventInfo->mcChannelNumber() << "," << m_eventInfo->eventNumber() << "," <<  truthPTV / 1e3  <<  "," << m_weight << "," << W_vecT.Pt() / 1e3  << std::endl;
    fileMerged.close();
  }
```
and similarly for the resolved analysis.  
Add more conditions depending on the kind of study, e.g. `AnalysisReader_VHQQ::STXS_GetPtVFromEvtInfo() > 150000 &&  tagcatExcl == 2 && ( nJet == 2 || nJet == 3)`.  
Consider including the cut on ptvtruth>150 GeV for the resolved (and merged) analysis to avoid saving huge files.

